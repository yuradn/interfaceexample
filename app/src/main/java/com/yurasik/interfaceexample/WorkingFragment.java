package com.yurasik.interfaceexample;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ���� on 17.09.2015.
 */
public class WorkingFragment extends Fragment {

    private GetInfo getInfo;
    private Handler handler;
    private Runnable runnable;
    private int step=0;

    @Bind(R.id.tvInfo)
    protected TextView tvInfo;

    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, v);
        getInfo = (GetInfo) getActivity();
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        working();
    }

    private void working(){
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                work();
            }
        };
        handler.postDelayed(runnable, TimeUnit.SECONDS.toMillis(1));
    }

    private void work(){
        step++;
        if (getView()!=null) {
            if (step%10==0) tvInfo.setText("**** "+step+" *****");
        }
        String str = "Step: " + step;
        Log.d(this.toString(), str);
        getInfo.getText(str);
        handler.postDelayed(runnable, TimeUnit.SECONDS.toMillis(1));
    }

    public interface GetInfo {
        public void getText(String info);
    }
}
