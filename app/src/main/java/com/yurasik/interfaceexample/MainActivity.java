package com.yurasik.interfaceexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements WorkingFragment.GetInfo {

    @Bind(R.id.tvInfo)
    protected TextView tvInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        WorkingFragment workingFragment = (WorkingFragment) getSupportFragmentManager()
                .findFragmentByTag(WorkingFragment.class.getCanonicalName());
        if (workingFragment==null) {
            workingFragment = new WorkingFragment();
            getSupportFragmentManager().beginTransaction()
                .add(R.id.infoContainer, workingFragment, WorkingFragment.class.getCanonicalName())
        .commit();
        }
    }

    @Override
    public void getText(String info) {
        tvInfo.setText(info);
    }

}
